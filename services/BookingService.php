<?php

namespace app\services;

use app\models\BookingDate;
use DateTime;
use Yii;
use Exception;
use app\models\Booking;
use app\dto\BookingDto;

class BookingService
{
    /**
     * @throws Exception
     */
    public function create(BookingDto $dto)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $booking = new Booking();
            $booking->client_id = 1;
            if (!$booking->save()) {
                throw new Exception('Ошибка при создании брони');
            }
            
            $checkInDate = new DateTime($dto->checkInDate);
            $checkOutDate = new DateTime($dto->checkOutDate);
            
            for ($dateIncrement = $checkInDate; $dateIncrement < $checkOutDate;) {
                $bookingDate = new BookingDate();
                $bookingDate->booking_id = $booking->id;
                $bookingDate->room_id = $dto->roomId;
                $bookingDate->date_from = $dateIncrement->format('Y-m-d');
                $bookingDate->date_to = $dateIncrement->modify('+1 day')->format('Y-m-d');
                if (!$bookingDate->save()) {
                    throw new Exception('Ошибка при создании брони');
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception('Ошибка при бронировании');
        }
    }
}
