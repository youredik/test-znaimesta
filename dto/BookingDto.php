<?php

namespace app\dto;

use yii\base\BaseObject;

class BookingDto extends BaseObject
{
    public int $roomCategoryId;
    public int $roomId;
    public string $checkInDate;
    public string $checkOutDate;
    public string $clientName;
    public string $clientEmail;
    
    public function getProperties(): array
    {
        return get_object_vars($this);
    }
}
