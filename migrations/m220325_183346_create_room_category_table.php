<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%room_category}}`.
 */
class m220325_183346_create_room_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%room_category}}',
            [
                'id'              => $this->primaryKey(),
                'name'            => $this->string(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%room_category}}');
    }
}
