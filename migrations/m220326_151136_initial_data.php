<?php

use yii\db\Migration;

/**
 * Class m220326_151136_initial_data
 */
class m220326_151136_initial_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(
            '{{%room_category}}',
            ['name'],
            [
                ['Одноместный'],
                ['Двухместный'],
                ['Люкс'],
                ['Де-Люк'],
            ]
        );
        
        $this->batchInsert(
            '{{%room}}',
            ['room_category_id'],
            [
                [1],
                [1],
                [2],
                [3],
                [3],
                [3],
                [4],
                [4],
                [4],
                [4],
                [4],
            ]
        );
        
        $this->batchInsert(
            '{{%client}}',
            ['name', 'email'],
            [
                ['Василий', 'vasya@mail.ru'],
            ]
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220326_151136_initial_data cannot be reverted.\n";
        
        return false;
    }
}
