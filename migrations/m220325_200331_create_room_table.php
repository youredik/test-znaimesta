<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%room}}`.
 */
class m220325_200331_create_room_table extends Migration
{
    private string $tableName = '{{%room}}';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id'               => $this->primaryKey(),
                'room_category_id' => $this->integer(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
        );
        
        $this->createIndex(
            'room_room_category_id_index',
            $this->tableName,
            'room_category_id',
        );
        
        $this->addForeignKey(
            'room_room_category_id_fk',
            $this->tableName,
            'room_category_id',
            '{{%room_category}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('room_room_category_id_index', $this->tableName);
        $this->dropForeignKey('room_room_category_id_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
