<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booking_date}}`.
 */
class m220326_075307_create_booking_date_table extends Migration
{
    private string $tableName = '{{%booking_date}}';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'booking_id' => $this->integer(),
                'room_id'    => $this->integer(),
                'date_from'  => $this->date(),
                'date_to'    => $this->date(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
        );
        
        $this->createIndex(
            'booking_date_booking_id_index',
            $this->tableName,
            'booking_id',
        );
        
        $this->createIndex(
            'booking_date_room_id_index',
            $this->tableName,
            'room_id',
        );
        
        $this->createIndex(
            'booking_date_date_from_index',
            $this->tableName,
            'date_from',
        );
        
        $this->createIndex(
            'booking_date_date_to_index',
            $this->tableName,
            'date_to',
        );
        
        $this->createIndex(
            'booking_date_room_id_date_from_date_to_uindex',
            $this->tableName,
            ['room_id', 'date_from', 'date_to'],
            true
        );
        
        $this->addForeignKey(
            'booking_date_booking_id_fk',
            $this->tableName,
            'booking_id',
            '{{%booking}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'booking_date_room_id_fk',
            $this->tableName,
            'room_id',
            '{{%room}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('booking_date_room_id_fk', $this->tableName);
        $this->dropForeignKey('booking_date_booking_id_fk', $this->tableName);
        
        $this->dropIndex('booking_date_room_id_date_from_date_to_uindex', $this->tableName);
        $this->dropIndex('booking_date_date_to_index', $this->tableName);
        $this->dropIndex('booking_date_date_from_index', $this->tableName);
        $this->dropIndex('booking_date_room_id_index', $this->tableName);
        $this->dropIndex('booking_date_booking_id_index', $this->tableName);
        
        $this->dropTable($this->tableName);
    }
}
