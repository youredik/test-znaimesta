<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booking}}`.
 */
class m220326_074641_create_booking_table extends Migration
{
    private string $tableName = '{{%booking}}';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id'        => $this->primaryKey(),
                'client_id' => $this->integer(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
        );
        
        $this->createIndex(
            'booking_client_id_index',
            $this->tableName,
            'client_id',
        );
        
        $this->addForeignKey(
            'booking_client_id_fk',
            $this->tableName,
            'client_id',
            '{{%client}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('booking_client_id_index', $this->tableName);
        $this->dropForeignKey('booking_client_id_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
