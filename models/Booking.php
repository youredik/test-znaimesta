<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int|null $client_id
 *
 * @property BookingDate[] $bookingDates
 * @property Client $client
 * @property RoomCategory $roomCategory
 */
class Booking extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['client_id'], 'integer'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
        ];
    }

    /**
     * Gets query for [[BookingDates]].
     *
     * @return ActiveQuery
     */
    public function getBookingDates(): ActiveQuery
    {
        return $this->hasMany(BookingDate::class, ['booking_id' => 'id']);
    }

    /**
     * Gets query for [[Client]].
     *
     * @return ActiveQuery
     */
    public function getClient(): ActiveQuery
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }
    
    /**
     * Gets query for [[BookingDates]].
     *
     * @return ActiveQuery
     */
    public function getBookingDate(): ActiveQuery
    {
        return $this->hasOne(BookingDate::class, ['booking_id' => 'id']);
    }
    
    /**
     * Gets query for [[Client]].
     *
     * @return ActiveQuery
     */
    public function getRoom(): ActiveQuery
    {
        return $this->hasOne(Room::class, ['id' => 'room_id'])
            ->via('bookingDate');
    }
    
    /**
     * Gets query for [[Client]].
     *
     * @return ActiveQuery
     */
    public function getRoomCategory(): ActiveQuery
    {
        return $this->hasOne(RoomCategory::class, ['id' => 'room_category_id'])
            ->via('room');
    }
}
