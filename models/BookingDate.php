<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "booking_date".
 *
 * @property int|null    $booking_id
 * @property int|null    $room_id
 * @property string|null $date_from
 * @property string|null $date_to
 *
 * @property Booking     $booking
 * @property Room        $room
 */
class BookingDate extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'booking_date';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['booking_id', 'room_id', 'date_from', 'date_to'], 'required'],
            [['booking_id', 'room_id'], 'integer'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
            [['room_id', 'date_from', 'date_to'], 'unique', 'targetAttribute' => ['room_id', 'date_from', 'date_to']],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::class, 'targetAttribute' => ['booking_id' => 'id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::class, 'targetAttribute' => ['room_id' => 'id']],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'booking_id' => 'Booking ID',
            'room_id'    => 'Room ID',
            'date_from'  => 'Date From',
            'date_to'    => 'Date To',
        ];
    }
    
    /**
     * Gets query for [[Booking]].
     *
     * @return ActiveQuery
     */
    public function getBooking(): ActiveQuery
    {
        return $this->hasOne(Booking::class, ['id' => 'booking_id']);
    }
    
    /**
     * Gets query for [[Room]].
     *
     * @return ActiveQuery
     */
    public function getRoom(): ActiveQuery
    {
        return $this->hasOne(Room::class, ['id' => 'room_id']);
    }
}
