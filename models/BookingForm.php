<?php

namespace app\models;

use app\dto\BookingDto;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Форма бронирования
 */
class BookingForm extends Model
{
    public $roomCategoryId;
    public $checkInDate;
    public $checkOutDate;
    public $clientName;
    public $clientEmail;
    public $roomId;
    
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['roomCategoryId', 'checkInDate', 'checkOutDate', 'clientName', 'clientEmail'], 'required'],
            [['roomCategoryId'], 'integer'],
            [['roomCategoryId'], 'exist', 'skipOnError' => true, 'targetClass' => RoomCategory::class, 'targetAttribute' => ['roomCategoryId' => 'id']],
            [['checkInDate', 'checkOutDate'], 'date', 'format' => 'php:Y-m-d'],
            [['clientName', 'clientEmail'], 'string'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'checkInDate'  => 'Дата заезда',
            'checkOutDate' => 'Дата выезда',
            'clientName'   => 'Ваше имя',
            'clientEmail'  => 'Email',
        ];
    }
    
    public function afterValidate()
    {
        if ($room = $this->findFreeRoom()) {
            $this->roomId = $room->id;
        } else {
            $this->addError('roomCategoryId', 'К сожалению, по заданным параметрам нет свободных номеров');
        }
        parent::afterValidate();
    }
    
    /**
     * Поиск свободного номера для выбранного периода дат и категории номера
     *
     * @return Room|array|ActiveRecord|null
     */
    private function findFreeRoom()
    {
        $subQuery = BookingDate::find()
            ->select('room_id')
            ->where(
                [
                    'OR',
                    ':dateFrom BETWEEN date_from AND date_to',
                    ':dateTo BETWEEN date_from AND date_to',
                    'date_from BETWEEN :dateFrom AND :dateTo',
                    'date_to BETWEEN :dateFrom AND :dateTo',
                ],
                [':dateFrom' => $this->checkInDate, ':dateTo' => $this->checkOutDate]
            )
            ->groupBy('room_id');
        
        return Room::find()
            ->leftJoin(['booking_date' => $subQuery], 'room.id = booking_date.room_id')
            ->andWhere(['booking_date.room_id' => null])
            ->andWhere(['room_category_id' => $this->roomCategoryId])
            ->one();
    }
    
    public function getDto(): BookingDto
    {
        return new BookingDto($this->attributes);
    }
}
