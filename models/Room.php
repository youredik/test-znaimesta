<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "room".
 *
 * @property int           $id
 * @property int|null      $room_category_id
 *
 * @property BookingDate[] $bookingDates
 * @property RoomCategory  $roomCategory
 */
class Room extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'room';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['room_category_id'], 'integer'],
            [['room_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoomCategory::class, 'targetAttribute' => ['room_category_id' => 'id']],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id'               => 'ID',
            'room_category_id' => 'Room Category ID',
        ];
    }
    
    /**
     * Gets query for [[BookingDates]].
     *
     * @return ActiveQuery
     */
    public function getBookingDates(): ActiveQuery
    {
        return $this->hasMany(BookingDate::class, ['room_id' => 'id']);
    }
    
    /**
     * Gets query for [[RoomCategory]].
     *
     * @return ActiveQuery
     */
    public function getRoomCategory(): ActiveQuery
    {
        return $this->hasOne(RoomCategory::class, ['id' => 'room_category_id']);
    }
}
