<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RoomSearch
 */
class RoomSearch extends Model
{
    public $checkInDate;
    public $checkOutDate;
    
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['checkInDate', 'checkOutDate'], 'required'],
            [['checkInDate', 'checkOutDate'], 'trim'],
            [['checkInDate', 'checkOutDate'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }
    
    public function attributeLabels(): array
    {
        return [
            'checkInDate'  => 'Дата заезда',
            'checkOutDate' => 'Дата выезда',
        ];
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $subQuery = BookingDate::find()
            ->select('room_id')
            ->groupBy('room_id');
        
        $subQuery2 = Room::find()
            ->select([
                         'room_category_id',
                         'total_number_of_rooms'  => 'count(room.id)',
                         'number_of_booked_rooms' => 'count(room_id)',
                     ])
            ->leftJoin(['booking_date' => $subQuery], 'room.id = booking_date.room_id')
            ->groupBy('room_category_id');
        
        $query = RoomCategory::find()
            ->select([
                         'room_category.*',
                         'total_number_of_rooms',
                         'number_of_booked_rooms',
                     ])
            ->leftJoin(['room' => $subQuery2], 'room.room_category_id = room_category.id');
        
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        
        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }
        
        $subQuery->where(
            [
                'OR',
                ':dateFrom BETWEEN date_from AND date_to',
                ':dateTo BETWEEN date_from AND date_to',
                'date_from BETWEEN :dateFrom AND :dateTo',
                'date_to BETWEEN :dateFrom AND :dateTo',
            ],
            [':dateFrom' => $this->checkInDate, ':dateTo' => $this->checkOutDate]
        );
        
        return $dataProvider;
    }
}
