<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "room_category".
 *
 * @property int         $id
 * @property string|null $name
 *
 * @property Room[]      $rooms
 */
class RoomCategory extends ActiveRecord
{
    /**
     * @var int
     *
     * Общее кол-во номеров
     */
    public $total_number_of_rooms;
    /**
     * @var int
     *
     * Кол-во забронированных номеров
     */
    public $number_of_booked_rooms;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'room_category';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }
    
    /**
     * Gets query for [[Rooms]].
     *
     * @return ActiveQuery
     */
    public function getRooms(): ActiveQuery
    {
        return $this->hasMany(Room::class, ['room_category_id' => 'id']);
    }
}
