<?php

use app\models\Booking;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $bookingList Booking[] */

$this->title = 'Забронированные номера';
$this->params['breadcrumbs'][] = ['label' => 'Поиск номеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <p class="font-weight-bold">Номер брони</p>
            </div>
            <div class="col-sm">
                <p class="font-weight-bold">Номер</p>
            </div>
            <div class="col-sm">
                <p class="font-weight-bold">Имя | Email</p>
            </div>
            <div class="col-sm">
                <p class="font-weight-bold">Дата въезда</p>
            </div>
            <div class="col-sm">
                <p class="font-weight-bold">Дата въезда</p>
            </div>
        </div>
    </div>
    <?php
    foreach ($bookingList as $booking): ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <p><?= $booking->id ?></p>
                </div>
                <div class="col-sm">
                    <p><?= $booking->roomCategory->name ?></p>
                </div>
                <div class="col-sm">
                    <p><?= $booking->client->name ?> | <?= $booking->client->email ?> </p>
                </div>
                <div class="col-sm">
                    <p><?= $booking->bookingDates[0]->date_from ?></p>
                </div>
                <div class="col-sm">
                    <p><?= $booking->bookingDates[count($booking->bookingDates) - 1]->date_to ?></p>
                </div>
            </div>
        </div>
    <?php
    endforeach; ?>

</div>
