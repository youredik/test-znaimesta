<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск номеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('roomBooked')): ?>
        <div class="alert alert-success">
            Номер успешно забронирован
        </div>
    <?php endif; ?>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <hr>
    <?= Html::a('Мои брони', ['booked']); ?>
    <hr>
    
    <?php if ($searchModel->validate()): ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <p class="font-weight-bold">Номера</p>
                </div>
                <div class="col-sm">
                    <p class="font-weight-bold">Всего номеров</p>
                </div>
                <div class="col-sm">
                    <p class="font-weight-bold">Кол-во доступных</p>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
        
        <?= ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'itemOptions'  => ['class' => 'item'],
                'layout'       => "{items}\n{pager}\n{summary}",
                'itemView'     => '_room',
                'viewParams'   => [
                    'checkInDate' => $searchModel->checkInDate,
                    'checkOutDate' => $searchModel->checkOutDate,
                ],
            ]
        ) ?>
    <?php endif; ?>

</div>
