<?php

use yii\helpers\Html;

use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RoomSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-search">

    <div class="alert alert-secondary" role="alert">
        Выберите даты заезда и выезда (формат yyyy-MM-dd)
        <br>
        Например: 2022-03-08
    </div>
    
    <?php
    $form = ActiveForm::begin(
        [
            'action' => ['index'],
            'method' => 'get',
            'layout' => ActiveForm::LAYOUT_INLINE,
        ]
    ); ?>
    
    <?= $form->field($model, 'checkInDate') ?>
    <?= $form->field($model, 'checkOutDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php
    ActiveForm::end(); ?>
    
    <?= $form->errorSummary($model) ?>

</div>
