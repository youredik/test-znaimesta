<?php

use app\models\RoomCategory;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model RoomCategory */
/* @var $checkInDate string */
/* @var $checkOutDate string */
?>

<div class="container">
    <div class="row">
        <div class="col-sm">
            <p><?= $model->name ?></p>
        </div>
        <div class="col-sm">
            <p><?= $model->total_number_of_rooms ?></p>
        </div>
        <div class="col-sm">
            <p><?= ($model->total_number_of_rooms - $model->number_of_booked_rooms) ?></p>
        </div>
        <div class="col-sm">
            <?= Html::a(
                'Выбрать',
                ['book', 'roomCategoryId' => $model->id, 'checkInDate' => $checkInDate, 'checkOutDate' => $checkOutDate],
                ['class' => 'btn btn-primary btn-sm']
            ); ?>
        </div>
    </div>
</div>

