<?php

use yii\helpers\Html;
use app\models\BookingForm;

use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $bookingForm BookingForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $roomCategoryName string */

$this->title = 'Бронирование номера';
$this->params['breadcrumbs'][] = ['label' => 'Поиск номеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="booking-form">

        <p class="font-weight-bold">Номер: <?= $roomCategoryName ?></p>
        <p class="font-weight-bold">Даты пребывания: <?= $bookingForm->checkInDate ?> - <?= $bookingForm->checkOutDate ?></p>
        
        <?php
        $form = ActiveForm::begin(
            [
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-4',
                    ],
                
                ],
            ]
        ); ?>
        
        <?= $form->field($bookingForm, 'clientName') ?>
        <?= $form->field($bookingForm, 'clientEmail') ?>

        <div class="form-group">
            <?= Html::submitButton('Бронировать', ['class' => 'btn btn-success']) ?>
        </div>
        
        <?php
        ActiveForm::end(); ?>

    </div>

</div>
