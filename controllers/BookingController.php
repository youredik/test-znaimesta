<?php

namespace app\controllers;

use app\models\Booking;
use Yii;
use Exception;
use yii\web\Response;
use yii\web\Controller;
use app\models\RoomSearch;
use app\models\BookingForm;
use app\models\RoomCategory;
use app\services\BookingService;
use yii\web\BadRequestHttpException;

/**
 * BookingController
 */
class BookingController extends Controller
{
    /**
     * Поиска доступных номеров
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new RoomSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Бронирование номера
     *
     * @return string|Response
     * @throws BadRequestHttpException
     */
    public function actionBook()
    {
        $bookingForm = new BookingForm();
        $bookingForm->load($this->request->queryParams, '');
        
        if (!$bookingForm->validate(['roomCategoryId', 'checkInDate', 'checkOutDate'])) {
            throw new BadRequestHttpException('Неверно переданы параметры...');
        }
        
        if ($this->request->isPost && $bookingForm->load(Yii::$app->request->post())) {
            if ($bookingForm->validate()) {
                try {
                    $service = new BookingService();
                    $service->create($bookingForm->getDto());
                    
                    Yii::$app->session->setFlash('roomBooked');
                    
                    return $this->redirect(['index']);
                } catch (Exception $e) {
                    throw new BadRequestHttpException('Ошибка при бронировании');
                }
            }
        }
        
        $roomCategory = RoomCategory::findOne($bookingForm->roomCategoryId);
        
        return $this->render('book', [
            'bookingForm'      => $bookingForm,
            'roomCategoryName' => $roomCategory->name,
        ]);
    }
    
    /**
     * Мои брони
     *
     * @return string
     */
    public function actionBooked(): string
    {
        $bookingList = Booking::find()->all();
        
        return $this->render('booked', [
            'bookingList' => $bookingList,
        ]);
    }
}
